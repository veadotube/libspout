#include <stdio.h>
#include "sdk\Spout.h"
#include <d3d11.h>
#include <dxgiformat.h>

class Sender
{
public:
	virtual ~Sender() {}
	virtual void Update(void* handle, uint32_t width, uint32_t height) = 0;
};

class SenderGL : public Sender {
private:
	Spout* spout;
public:
	SenderGL(const char* name)
	{
		spout = new Spout;
		spout->SetAutoShare();
		spout->SetSenderName(name);
	}
	~SenderGL()
	{
		delete spout;
	}
	void Update(void* handle, uint32_t width, uint32_t height)
	{
		spout->SendFbo((GLuint)handle, width, height, true);
	}
};

class SenderDX11 : public Sender {
private:
	char name[256];
	spoutSenderNames names;
	spoutFrameCount frame;
	spoutDirectX dx;
	bool init;
	ID3D11Device* device;
	ID3D11Texture2D* shared;
	HANDLE sharedHandle;
	UINT width, height;
public:
	SenderDX11(const char* name)
	{
		init = false;
		device = 0;
		shared = 0;
		sharedHandle = 0;
		width = height = 0;
		char baseName[256] = {};
		if (name && strnlen_s(name, 256) > 0)
		{
			strcpy_s(baseName, 256, name);
		}
		else
		{
			strcpy_s(baseName, 256, GetExeName().c_str());
		}
		strcpy_s(this->name, 256, baseName);
		int i = 0;
		names.CleanSenders();
		while (names.FindSenderName(this->name)) sprintf_s(this->name, "%s (%d)", baseName, ++i);
		names.RegisterSenderName(this->name);
		frame.CreateAccessMutex(this->name);
	}
	~SenderDX11()
	{
		frame.CleanupFrameCount();
		frame.CloseAccessMutex();
		names.ReleaseSenderName(name);
		names.CleanSenders();
		if (shared) shared->Release();
		if (device) device->Release();
	}
	void Update(void* handle, uint32_t width, uint32_t height)
	{
		auto texture = (ID3D11Texture2D*)handle;
		ID3D11Device* device = 0;
		D3D11_TEXTURE2D_DESC desc = {};
		texture->GetDevice(&device);
		texture->GetDesc(&desc);
		if (this->device != device || this->width != desc.Width || this->height != desc.Height)
		{
			if (this->device) this->device->Release();
			this->device = device;
			this->device->AddRef();
			this->width = desc.Width;
			this->height = desc.Height;
			dx.CreateSharedDX11Texture(device, desc.Width, desc.Height, desc.Format, &shared, sharedHandle);
			if (!init)
			{
				names.CreateSender(this->name, desc.Width, desc.Height, sharedHandle, (DWORD)desc.Format);
				frame.EnableFrameCount(this->name);
				init = true;
			}
			else
			{
				names.UpdateSender(this->name, desc.Width, desc.Height, sharedHandle, (DWORD)desc.Format);
			}
		}
		ID3D11DeviceContext* context;
		if (frame.CheckTextureAccess(shared))
		{
			device->GetImmediateContext(&context);
			context->CopyResource(shared, texture);
			context->Flush();
			frame.SetNewFrame();
			frame.AllowTextureAccess(shared);
			context->Release();
		}
		device->Release();
	}
};

#define EXPORT(X) extern "C" __declspec(dllexport) X WINAPI

#define SPOUT_MODE_GL 10
#define SPOUT_MODE_DX11 20

EXPORT(Sender*) spout_sender_create(const char* name, uint32_t mode)
{
	switch (mode)
	{
	case SPOUT_MODE_GL: return new SenderGL(name);
	case SPOUT_MODE_DX11: return new SenderDX11(name);
	default: return 0;
	}
}
EXPORT(void) spout_sender_update(Sender* s, void* handle, uint32_t width, uint32_t height)
{
	if (s) s->Update(handle, width, height);
}
EXPORT(void) spout_sender_destroy(Sender* s)
{
	if (s) delete s;
}